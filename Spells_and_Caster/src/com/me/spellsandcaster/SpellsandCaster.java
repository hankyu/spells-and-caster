package com.me.spellsandcaster;

import com.badlogic.gdx.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.spellsandcaster.components.ActorComponent;
import com.spellsandcaster.components.PlayerMovementComponent;
import com.spellsandcaster.entities.Entity;
import com.spellsandcaster.entities.EntityID;
import com.spellsandcaster.screens.SplashScreen;
import com.spellsandcaster.system.MovementSystem;
import com.spellsandcaster.system.World;

public class SpellsandCaster extends Game {
	
	// splash screen
	private static final String TAG = "SpellsandCaster";
	private World world;
	private Stage stage;
	
	@Override
	public void create() {
		Gdx.app.log(TAG, "On Create.");
		setScreen(new SplashScreen(this));
		
		// setup world
		world = new World(this);
		world.addSystem(new MovementSystem());
		
		// test object
		Entity Player = world.createEntity(EntityID.PLAYER, true);
		Player.getComponentManager().addComponent(new ActorComponent(new Texture("data/Player.png"), 400, 320));
		//Player.getComponentManager().addComponent(new PositionComponent(500, 100));
		Player.getComponentManager().addComponent(new PlayerMovementComponent());
		
		if(stage == null)
			stage = new Stage(Gdx.app.getGraphics().getWidth(),
							  Gdx.app.getGraphics().getHeight(),
							  true);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
	
	public World getWorld() {
		return world;
	}
	
	public Stage getStage() {
		return stage;
	}
	
	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
