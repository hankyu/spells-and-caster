package com.spellsandcaster.system;

import java.util.LinkedList;

import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.entities.Entity;

public class SystemManager {

	private LinkedList<ISystem> systems;
	
	public SystemManager() {
		systems = new LinkedList<ISystem>();
	}
	
	public void addSystem(ISystem system) {
		systems.add(system);
	}
	
	public void removeSystem(ISystem system) {
		systems.remove(system);
	}
	
	public ISystem getSystem(int index) {
		return systems.get(index);
	}
	
	public void update(LinkedList<Entity> entityList, SpellsandCaster game) {
		
		for(Entity entity : entityList) {
			for(ISystem system : systems) {
				system.update(entity, game);
			}
		}
	}
}
