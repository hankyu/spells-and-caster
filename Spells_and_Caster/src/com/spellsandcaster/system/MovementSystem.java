package com.spellsandcaster.system;

import java.util.HashMap;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.components.IComponent;
import com.spellsandcaster.components.PositionComponent;
import com.spellsandcaster.entities.Entity;
import com.spellsandcaster.tweenaccessors.ActorTween;

public class MovementSystem implements ISystem{
	
	public static final String TAG = "MovementSystem";
	
	private HashMap<String, Integer> movementType;
	
	public MovementSystem() {
		movementType = new HashMap<String, Integer>();
		movementType.put("PlayerMovementComponent", 0);
	}

	@Override
	public void update(Entity entity, SpellsandCaster game) {
		float x, y;
		try {
			x = PositionComponent.class.cast(entity.getComponentManager().getComponentByType("PositionComponent")).getX();
			y = PositionComponent.class.cast(entity.getComponentManager().getComponentByType("PositionComponent")).getY();
			
		} catch(NullPointerException ex) {
			
		}
		
		int type = -1;
		
		FindType:
		for(IComponent c : entity.getComponentManager().getComponentList()) {
			if(movementType.containsKey(c.getClass().getSimpleName())) {
				type = movementType.get(c.getClass().getSimpleName());
				break FindType;
			}
		}
		
		switch(type) {
			// player movement
			case 0:
				playerMovement(entity);
				break;
			default:
				assert false;
				Gdx.app.error(TAG, "Missing component");
				break;
		}
	}
	
	// move to touched place
	private void playerMovement(Entity e) {
		Actor actor = Actor.class.cast(e.getComponentManager().getComponentByType("ActorComponent"));
		
		if(Gdx.app.getInput().isTouched()) {
			//Gdx.app.log(TAG, "playerMovement " + actor.getX() + " : " + actor.getY());
			
			float x = Gdx.app.getInput().getX(),
				  y = Gdx.app.getGraphics().getHeight() - Gdx.app.getInput().getY();
			
			//Gdx.app.log(TAG, "playerMovement " + x + " : " + y);
			/*
			actor.addAction(Actions.moveTo(x - actor.getWidth() / 2f, 
										   y - actor.getHeight() / 2f, 
										   0.75f));
										   */
			//Tween.registerAccessor(Actor.class, new ActorTween());
			float x1, x2, y1, y2;
			x1 = x - actor.getWidth() / 2f;
			y1 = y - actor.getHeight() / 2f;
			x2 = actor.getX() - actor.getWidth() / 2f;
			y2 = actor.getY() - actor.getHeight() / 2f;
			double dist = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow(y1 - y2, 2));
			float time = (float) (dist / 50f);

			actor.addAction(Actions.moveTo(x - actor.getWidth() / 2f, 
					   y - actor.getHeight() / 2f, 
					   time));
		}
		else {
			actor.getActions().clear();
		}
		
	}
}
