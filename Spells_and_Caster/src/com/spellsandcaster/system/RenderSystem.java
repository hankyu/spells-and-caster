package com.spellsandcaster.system;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.spellsandcaster.components.PositionComponent;
import com.spellsandcaster.components.RenderComponent;
import com.spellsandcaster.entities.Entity;

public class RenderSystem implements ISystem{

	SpriteBatch SystemBatch = new SpriteBatch();
	
	@Override
	public void update(Entity entity) {
		
		// get position
		float x, y;
		
		x = PositionComponent.class.cast(entity.getComponentManager().getComponentByType("PositionComponent")).getX();
		y = PositionComponent.class.cast(entity.getComponentManager().getComponentByType("PositionComponent")).getY();
		
		RenderComponent.class.cast(entity.getComponentManager().getComponentByType("RenderComponent")).drawToBatch(x, y, SystemBatch);
		
	}

}
