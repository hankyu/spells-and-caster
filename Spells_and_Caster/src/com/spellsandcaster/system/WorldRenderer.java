package com.spellsandcaster.system;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.spellsandcaster.components.ActorComponent;
import com.spellsandcaster.components.PositionComponent;
import com.spellsandcaster.components.RenderComponent;
import com.spellsandcaster.entities.Entity;

public class WorldRenderer {
	
	public static String TAG = "WorldRenderer";
	
	public void update(LinkedList<Entity> entityList, SpriteBatch batch, Stage stage) {
		//Gdx.app.log(TAG, "updating");
		batch.begin();
		stage.draw();
		for(Entity e : entityList) {
			//Gdx.app.log(TAG, "Rendering entity" + e.getEntityID());
			// get position
			float x = 0, y = 0;
			try {
				x = PositionComponent.class.cast(e.getComponentManager().getComponentByType("PositionComponent")).getX();
				y = PositionComponent.class.cast(e.getComponentManager().getComponentByType("PositionComponent")).getY();
				
			} catch(NullPointerException ex) {
				
			}
			
			// try to draw sprite
			if(e.isActor()) {
				try {
					
					Actor actor
						= ActorComponent.class.cast(e.getComponentManager().getComponentByType("ActorComponent"));
					//actor.setPosition(x, y);
					actor.draw(batch, batch.getColor().a);
					
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			else {
				try {
					RenderComponent.class.cast(e.getComponentManager().getComponentByType("RenderComponent")).drawToBatch(x, y).draw(batch);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		batch.end();
	}

}
