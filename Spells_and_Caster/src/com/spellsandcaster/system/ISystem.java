package com.spellsandcaster.system;

import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.entities.Entity;

public interface ISystem {
	
	public void update(Entity entity, SpellsandCaster game);
}
