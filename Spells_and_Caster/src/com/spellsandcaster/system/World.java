package com.spellsandcaster.system;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.entities.Entity;
import com.spellsandcaster.entities.EntityManager;

public class World {
	
	public static final String TAG = "World";
	
	private SpellsandCaster game;
	
	private EntityManager entityManager;
	private SystemManager systemManager;
	private WorldRenderer wrender;
	
	public World(SpellsandCaster game) {
		this.game = game;
		entityManager = new EntityManager();
		systemManager = new SystemManager();
		wrender = new WorldRenderer();
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public SpellsandCaster getGame() {
		return game;
	}
	
	public Entity createEntity(int EntityID, boolean isActor) {
		Entity entity = new Entity(EntityID, isActor);
		entityManager.addEntity(entity);
		return entity;
	}
	
	public void addSystem(ISystem system) {
		systemManager.addSystem(system);
	}
	
	public void updateWorld() {
		systemManager.update(entityManager.getEntityList(), game);
	}
	
	public void renderWorld(SpriteBatch batch) {
		//Gdx.app.log(TAG, "Render World");
		wrender.update(entityManager.getEntityList(), batch, game.getStage());
	}
}
