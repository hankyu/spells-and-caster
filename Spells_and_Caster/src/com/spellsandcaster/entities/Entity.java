package com.spellsandcaster.entities;

import com.spellsandcaster.components.ComponentManager;

public class Entity {
	
	private ComponentManager compManager;
	private final int EntityID;
	private boolean isActor;
	
	public Entity(int EntityID, boolean isActor) {
		compManager = new ComponentManager();
		this.EntityID = EntityID;
		this.isActor = isActor;
	}
	
	public ComponentManager getComponentManager() {
		return compManager;
	}
	
	public int getEntityID() {
		return EntityID;
	}
	
	public boolean isActor() {
		return isActor;
	}
}
