package com.spellsandcaster.entities;

import java.util.LinkedList;

public class EntityManager {
	
	private LinkedList<Entity> entities;
	
	public EntityManager() {
		entities = new LinkedList<Entity>();
	}
	
	public void addEntity(Entity entity) {
		entities.add(entity);
	}
	
	public void removeEntity(Entity entity) {
		entities.remove(entity);
	}
	
	public void clearEntity() {
		entities.clear();
	}
	
	public Entity getEntity(int index) {
		return entities.get(index);
	}
	
	public Entity getEntityById(int ID) {
		for(Entity e : entities) {
			if(e.getEntityID() == ID) {
				return e;
			}
		}
		return null;
	}
	
	public LinkedList<Entity> getEntityList() {
		return entities;
	}
}
