package com.spellsandcaster.screens;

import aurelienribon.tweenengine.BaseTween;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.tweenaccessors.SpriteTween;

public class SplashScreen implements Screen{

	private static final String TAG = "SplashScreen";
	
	private SpellsandCaster game;
	private Texture splashTexture;
	private Sprite splashSprite;
	private SpriteBatch batch;
	
	TweenManager manager;
	
	public SplashScreen(SpellsandCaster game) {
		this.game = game;
	}
	
	@Override
	public void render(float delta) {
		if(Gdx.app.getInput().isTouched()) {
			game.setScreen(new MainMenuScreen(game));
		}
		
		Gdx.gl.glClearColor(255, 255, 255, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		manager.update(delta);
		
		batch.begin();
		splashSprite.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		splashTexture = new Texture("data/splash.png");
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		splashSprite = new Sprite(splashTexture);
		splashSprite.setSize(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());
		splashSprite.setPosition(Gdx.graphics.getWidth()/2 - splashSprite.getWidth()/2, 
								 Gdx.graphics.getHeight()/2 - splashSprite.getHeight()/2);
		splashSprite.setColor(1, 1, 1, 0);
		batch = new SpriteBatch();
		
		TweenCallback tcb = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				TweenCompleted();
			}
		};
		
		// tweening
		Tween.registerAccessor(Sprite.class, new SpriteTween());
		manager = new TweenManager();
		Tween.to(splashSprite, SpriteTween.ALPHA, 2f)
			 .target(1)
			 .ease(TweenEquations.easeInQuad)
			 .repeatYoyo(1, 2.5f)
			 .setCallback(tcb)
			 .setCallbackTriggers(TweenCallback.COMPLETE)
			 .start(manager);
	}

	protected void TweenCompleted() {
		Gdx.app.log(TAG, "TweenYoyo Completed.");
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

}
