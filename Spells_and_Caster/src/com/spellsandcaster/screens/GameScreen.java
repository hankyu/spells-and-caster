package com.spellsandcaster.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.entities.EntityID;
import com.spellsandcaster.system.World;

public class GameScreen implements Screen{

	public static final String TAG = "GameScreen";
	
	private SpellsandCaster game;
	private Stage stage, pauseStage;
	private World world;
	private SpriteBatch batch;
	private Actor Player;
	private boolean isPaused;
	
	public GameScreen(SpellsandCaster game) {
		isPaused = false;
		this.game = game;
		this.stage = new Stage();
		pauseStage = new Stage();
		game.setStage(stage);
		world = game.getWorld();
		batch = new SpriteBatch();
		
		// add actor to this world
		Player = Actor.class.cast(world.getEntityManager()
						.getEntityById(EntityID.PLAYER)
						.getComponentManager()
						.getComponentByType("ActorComponent"));
		
		// add listener to player
		Player.addListener(new InputListener() {
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				super.touchUp(event, x, y, pointer, button);
				Gdx.app.log(TAG, "Player Touch-up");
			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
					Gdx.app.log(TAG, "Player Touch-down");
					pauseScreen();
					isPaused = true;
				return true;
			}
			
		});
		
		stage.addActor(Player);
		stage.setViewport(Gdx.app.getGraphics().getWidth(), 
						  Gdx.app.getGraphics().getHeight(), true);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(255, 255, 255, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// pause when player being pressed
		if(isPaused) {
			//Gdx.input.setInputProcessor(pauseStage);
			game.setStage(pauseStage);
			pauseStage.act(delta);
			world.updateWorld();
			world.renderWorld(batch);
		
			// TODO: now press "A" to get out of pause stage
			if(Gdx.app.getInput().isKeyPressed(Input.Keys.A)) {
				isPaused = false;
				Gdx.app.log(TAG, "out of pause");
			}
		}
		else {
			//Gdx.app.log(TAG, "running");
			Gdx.input.setInputProcessor(stage);
			game.setStage(stage);
			stage.act(delta);
			
			world.updateWorld();
			world.renderWorld(batch);
		}
		//Gdx.app.log(TAG, Gdx.app.getInput().getX() + " : " + Gdx.app.getInput().getY());
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();
		pauseStage.dispose();
	}
	
	protected void pauseScreen() {
		
	}
	
	public boolean isPaused() {
		return isPaused;
	}
	
	public void setPause(boolean isPaused) {
		this.isPaused = isPaused; 
	}
}
