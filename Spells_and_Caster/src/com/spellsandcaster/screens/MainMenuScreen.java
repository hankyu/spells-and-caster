package com.spellsandcaster.screens;

import aurelienribon.tweenengine.BaseTween;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.me.spellsandcaster.SpellsandCaster;
import com.spellsandcaster.tweenaccessors.ButtonTween;

public class MainMenuScreen implements Screen{

	private static final String TAG = "MainMenu";
	private static final int TCB_BTNSTARTGAME = 1,
							 TCB_BTNLOADGAME = 2;
	
	SpellsandCaster game;
	SpriteBatch batch;
	
	private BitmapFont fntZrnicBlack;
	TextureAtlas atlas;
	Skin skin;
	
	private float screenWidth, screenHeight;
	
	private Stage stage;
	
	private TextButton btnStartGame, btnLoadGame;
	
	private TweenManager manager;
	
	public MainMenuScreen(SpellsandCaster game) {
		this.game = game;
		batch = new SpriteBatch();
		screenWidth = Gdx.app.getGraphics().getWidth();
		screenHeight = Gdx.app.getGraphics().getHeight();
		
		if(stage == null) 
			stage = new Stage(screenWidth, screenHeight, true);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(255, 255, 255, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		manager.update(delta);
		
		stage.act(delta);
		
		batch.begin();
		stage.draw();
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
		batch = new SpriteBatch();
		skin = new Skin();
		atlas = new TextureAtlas("data/Button/button.atlas");
		skin.addRegions(atlas);
		fntZrnicBlack = new BitmapFont(Gdx.files.internal("data/font/Zrnic/zrnicBlack.fnt"), false);
		
		// setup stage
		Gdx.input.setInputProcessor(stage);
		
		// set up style
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("button");
		style.font = fntZrnicBlack;
		
		// button setting
		// new game
		btnStartGame = new TextButton("New Game", style);
		btnStartGame.setSize(100, 50);
		btnStartGame.setPosition(800, 160);
		// load game
		btnLoadGame = new TextButton("Load Game", style);
		btnLoadGame.setSize(100, 50);
		btnLoadGame.setPosition(800, 90);
		
		// tween button position
		manager = new TweenManager();
		
		TweenCallback btnStartGameCallback = new TweenCallback() {

			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted(TCB_BTNSTARTGAME);
			}
			
		};
		TweenCallback btnLoadGameCallback = new TweenCallback() {

			@Override
			public void onEvent(int type, BaseTween<?> source) {
				tweenCompleted(TCB_BTNLOADGAME);
			}
			
		};
		
		Tween.registerAccessor(TextButton.class, new ButtonTween());
		Tween.to(btnStartGame, ButtonTween.POSITION_X, 1f)
			 .target(625)
			 .ease(TweenEquations.easeOutQuad)
			 .setCallback(btnStartGameCallback)
			 .setCallbackTriggers(TweenCallback.COMPLETE)
			 .start(manager);
		
		Tween.to(btnLoadGame, ButtonTween.POSITION_X, 1f)
			 .delay(0.25f)
			 .target(625)
			 .ease(TweenEquations.easeOutQuad)
			 .setCallback(btnLoadGameCallback)
			 .setCallbackTriggers(TweenCallback.COMPLETE)
			 .start(manager);
		
		// add buttons to stage
		stage.addActor(btnStartGame);
		stage.addActor(btnLoadGame);
	}

	protected void tweenCompleted(int tcbBtnstartgame) {
		switch(tcbBtnstartgame) {
			case TCB_BTNSTARTGAME:
				btnStartGame.addListener(new InputListener() {
					@Override
					public void touchUp(InputEvent event, float x, float y,
							int pointer, int button) {
//						Gdx.app.log(TAG, "StartGame UP");
						TweenCallback startgameCallback = new TweenCallback() {
							
							@Override
							public void onEvent(int type, BaseTween<?> source) {
								startGame();
							}
						};
						Tween.to(btnStartGame, ButtonTween.POSITION_X, 1f)
							 .target(800)
							 .ease(TweenEquations.easeInCubic)
							 .start(manager);
						
						Tween.to(btnLoadGame, ButtonTween.POSITION_X, 1f)
							 .delay(0.25f)
							 .target(800)
							 .setCallback(startgameCallback)
							 .setCallbackTriggers(TweenCallback.COMPLETE)
							 .ease(TweenEquations.easeInCubic)
							 .start(manager);
						
					}

					@Override
					public boolean touchDown(InputEvent event, float x,
							float y, int pointer, int button) {
						return true;
					}
				});
				break;
			case TCB_BTNLOADGAME:
				btnLoadGame.addListener(new InputListener() {
					@Override
					public void touchUp(InputEvent event, float x, float y,
							int pointer, int button) {
//						Gdx.app.log(TAG, "LoadGame UP");
					}

					@Override
					public boolean touchDown(InputEvent event, float x,
							float y, int pointer, int button) {
						return true;
					}
				});
				break;
			default:
				assert false;
				break;
		}
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		skin.dispose();
		stage.dispose();
		fntZrnicBlack.dispose();
	}
	
	public void startGame() {
		Gdx.app.log(TAG, "Game Start.");
		game.setScreen(new GameScreen(game));
	}
}
