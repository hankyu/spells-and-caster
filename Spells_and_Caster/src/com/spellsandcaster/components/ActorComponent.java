package com.spellsandcaster.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActorComponent extends Actor implements IComponent{

	private Sprite sprite = null;
	
	public ActorComponent(Texture texture, float x, float y) {
		if(texture != null)
			sprite = new Sprite(texture);
		this.setPosition(x, y);
		this.setSize(texture.getWidth(), texture.getHeight());
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		sprite.setPosition(getX(), getY());
		sprite.draw(batch);
	}

	
}
