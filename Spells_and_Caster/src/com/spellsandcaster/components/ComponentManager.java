package com.spellsandcaster.components;

import java.util.LinkedList;


import com.badlogic.gdx.Gdx;

public class ComponentManager {
	
	private LinkedList<IComponent> components;
	
	public ComponentManager() {
		components = new LinkedList<IComponent>();
	}
	
	public void addComponent(IComponent c) {
		components.add(c);
	}
	
	public void removeComponent(IComponent c) {
		components.remove(c);
	}
	
	public void clearComponents() {

		components.clear();
	}
	
	public IComponent getComponent(int index) {
		return components.get(index);
	}
	
	public IComponent getComponentByType(String type) {
		for(IComponent c : components) {
			//Gdx.app.log("typeName", getSimpleName(c.getClass()));
			if(c.getClass().getSimpleName().compareTo(type) == 0) {
				
				return c;
			}
		}
		
		return null;
	}
	
	public String getType(int index) {
		IComponent comp = components.get(index);
		String simpleName = comp.getClass().getSimpleName();
		return simpleName;
	}
	
	public LinkedList<IComponent> getComponentList() {
		return components;
	}
}
