package com.spellsandcaster.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class RenderComponent implements IComponent{
	
	//private SpellsandCaster game;
	private Texture Texture;
	private Sprite Sprite;
	
	public RenderComponent(Texture texture) {
		Texture = texture;
		Sprite = new Sprite(Texture);
	}
	
	// draw for a sprite
	public Sprite drawToBatch(float x, float y) {
		
		Sprite.setX(x);
		Sprite.setY(y);
		
		return Sprite;
	}
	
	// draw actor
	public Sprite getSprite() {
		
		return Sprite;
	}
}
