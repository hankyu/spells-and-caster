package com.spellsandcaster.tweenaccessors;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActorTween implements TweenAccessor<Actor>{

	public static final int POSITION_X = 0,
							POSITION_Y = 1,
							POSITION_XY = 2;
	
	@Override
	public int getValues(Actor target, int tweenType, float[] returnValues) {
		switch(tweenType) {
			case POSITION_X:
				returnValues[0] = target.getX();
				return 1;
			case POSITION_Y:
				returnValues[0] = target.getY();
				return 1;
			case POSITION_XY:
				returnValues[0] = target.getX();
				returnValues[1] = target.getY();
				return 2;
			default:
				assert false;
				return -1;
		}
	}

	@Override
	public void setValues(Actor target, int tweenType, float[] newValues) {
		switch(tweenType) {
			case POSITION_X:
				target.setX(newValues[0]);
				break;
			case POSITION_Y:
				target.setY(newValues[0]);
				break;
			case POSITION_XY:
				target.setX(newValues[0]);
				target.setY(newValues[1]);
				break;
			default:
				assert false;
				break;
		}
	}

}
